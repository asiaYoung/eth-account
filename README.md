# eth-account

生成某尾号的地址

## 步骤

1. 修改文件 index.php

    ```js
    const tail = 'a'; // 目标，要得到的尾部字符串
    ```

2. 修改 install.sh 和 run.sh

    ```
    #修改这里 DIR=eth-account项目所在的上一级目录
    DIR=/Users/data/dev
    ```

3. 执行 ./install.sh

    如果有权限问题,执行: chmod 755 install.sh,后再执行./install.sh

4. 执行 ./run.sh

    权限问题同3处理