import Web3 from 'web3';

let web3 = new Web3('http://localhost:8545');
// 尾部匹配字符串： 要修改处tail = 'xxxx'
const tail = 'a';
(function() {
    let reg = new RegExp(`${tail}$`, 'i');
    let i = 0;
    while(1){
        i++
        console.log(`第${i}次...`);
        let ret = web3.eth.accounts.create();
        if(reg.test(ret.address)){
            console.log(ret);
            break;
        }
    }
})();


// node index.js || npm run start
