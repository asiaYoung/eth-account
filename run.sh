#!/usr/bin/env bash

set -e
set -o pipefail
#修改这里 DIR=eth-account项目所在的上一级目录
DIR=/Users/data/dev

image=asiayoung/node:build-16
name=eth-account

docker rm -f $name
docker run --name $name \
  -v $DIR/eth-account:/app \
  -w /app \
  $image npm run start